#!/usr/bin/python
from flask import Flask, render_template, url_for, redirect, request
from jinja2 import evalcontextfilter, Markup
from flask.ext.sqlalchemy import SQLAlchemy
from datetime import datetime, timedelta
from theapis import keyID, vCode
import eveapi
import math
import sys
import time
import random

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/contracts.db'
db = SQLAlchemy(app)
api = eveapi.EVEAPIConnection().auth(keyID=keyID,vCode=vCode)

class Contracts(db.Model):
   """
   Model stores an assumed courier contract and all information given throught the api

   issuer, issuerCorp, assignee, acceptor, startStation, and endStation will all be linked
   to the Parties model, which will grab the names for everything. But it's not done yet

   """
   contractID = db.Column(db.Integer, unique=True, primary_key=True)
   issuerID = db.Column(db.Integer, db.ForeignKey('parties.id'))
   issuer = db.relationship('Parties', primaryjoin='Parties.id==Contracts.issuerID')
   issuerCorpID = db.Column(db.Integer, db.ForeignKey('parties.id'))
   issuerCorp = db.relationship('Parties', primaryjoin='Parties.id==Contracts.issuerCorpID')
   assigneeID = db.Column(db.Integer, db.ForeignKey('parties.id'))
   assignee = db.relationship('Parties', primaryjoin='Parties.id==Contracts.assigneeID')
   acceptorID = db.Column(db.Integer, db.ForeignKey('parties.id'))
   acceptor = db.relationship('Parties', primaryjoin='Parties.id==Contracts.acceptorID')
   startStationID = db.Column(db.Integer, db.ForeignKey('parties.id'))
   startStation = db.relationship('Parties', primaryjoin='Parties.id==Contracts.startStationID')
   endStationID = db.Column(db.Integer, db.ForeignKey('parties.id'))
   endStation = db.relationship('Parties', primaryjoin='Parties.id==Contracts.endStationID')
   type = db.Column(db.String(50))
   status = db.Column(db.String(20))
   title = db.Column(db.String(200))
   dateIssued = db.Column(db.DateTime)
   dateExpired = db.Column(db.DateTime)
   dateAccepted = db.Column(db.DateTime)
   numDays = db.Column(db.Integer)
   dateCompleted = db.Column(db.DateTime)
   reward = db.Column(db.Float)
   volume = db.Column(db.Float)

   def __init__(self, row):
      self.contractID = row['contractID']
      self.issuerID = row['issuerID']
      self.issuerCorpID = row['issuerCorpID']
      self.assigneeID = row['assigneeID']
      self.acceptorID = row['acceptorID']
      self.startStationID = row['startStationID']
      self.endStationID = row['endStationID']
      self.type = row['type']
      self.status = row['status']
      self.title = row['title']
      self.numDays = row['numDays']
      self.reward = row['reward']
      self.volume = row['volume']
      self.update(row)

   def update(self, row):
      """
      updates the important parts of the contract

      """
      self.status = row['status']
      if row['dateIssued']:
         self.dateIssued = datetime.fromtimestamp(float(row['dateIssued']))
      if row['dateExpired']:
         self.dateExpired = datetime.fromtimestamp(float(row['dateExpired']))
         if datetime.now() > datetime.fromtimestamp(row['dateExpired']) and self.status == 'Outstanding':
            self.status = 'Expired'
      if row['dateAccepted']:
         self.dateAccepted = datetime.fromtimestamp(float(row['dateAccepted']))
      if row['dateCompleted']:
         self.dateCompleted = datetime.fromtimestamp(float(row['dateCompleted']))

   def summary(self):
      """
      gives a quick summary of the items in the row

      """
      keys = ['reward','volume','duration','title','dateCompleted']
      values = [self.reward,
                self.volume,
                total_seconds(self.dateCompleted - self.dateIssued),
                self.title,
                self.dateCompleted]

      result = dict(zip(keys, values))
      return result

   def duration(self):
      return total_seconds(self.dateCompleted - self.dateIssued)

   def ids(self):
      return [self.issuerID,
              self.issuerCorpID,
              self.assigneeID,
              self.acceptorID,
              self.startStationID,
              self.endStationID]

   def __repr__(self):
      return '<id %s>' % self.contractID

class Parties(db.Model):
   """
   Will store all names for an id

   """
   __tablename__ = 'parties'
   id = db.Column(db.Integer, primary_key=True)
   name = db.Column(db.String(500))

   def __init__(self, id):
      q = self.query.get(id)
      if id and q is None:
         self.id = id
         self.name = None
         db.session.add(self)
         db.session.commit()

   def __repr__(self):
      if self.name:
         return self.name
      else:
         return "<Party %s>" % self.id

class Updates(db.Model):
   """
   logs updates and more importantly cacheUntil timers

   """
   id = db.Column(db.Integer, primary_key=True)
   type = db.Column(db.String(10))
   cachedUntil = db.Column(db.DateTime)
   requestTime = db.Column(db.DateTime)
   note = db.Column(db.String(100))

   def __init__(self, requestTime, cachedUntil, type, note=None):
      self.cachedUntil = cachedUntil
      self.requestTime = requestTime
      self.type = type
      if note:
         self.note = note

   def __repr__(self):
      return '<Update id:%s %s>' % (self.id, self.type)

def worker():
   """
   checks update timers
   If time expired, update the contracts and/or parties
   I'd like to make this easier to read in the terminal

   """
   now = datetime.now()
   next_update = lambda x: Updates.query.filter(Updates.type==x).order_by(db.desc(Updates.id)).first().cachedUntil
   
   updates = {'contracts': update,
              'parties':   pull_parties,
              'lottery':   run_lottery}
   delta = list()

   for u, m in updates.items():
      if now > next_update(u):
         print '%s :: running %s update... ' % (time.strftime('%X'), u)
         m()
         print '%s :: ran %s update' % (time.strftime('%X'), u)
      d = total_seconds(next_update(u) - now)
      print '%s :: %s update in approx %s' % (time.strftime('%X'), u, strDuration(d)) 
      delta += [total_seconds(next_update(u) - now)]

   return min(delta)
 
def run_lottery():
   """
   Pulls all completed contracts from the last lottery run, filters out all
   of those that have a rate of less than 200isk/m3 (which can change)
   and picks a random one.

   """
   last = Updates.query.filter(Updates.type=='lottery').order_by(db.desc(Updates.id)).first()
   c = Contracts.query.filter(Contracts.status=='Completed')
   c = c.filter(Contracts.dateCompleted>=datetime.now() - timedelta(days=14))

   issuers = dict()
   for contract in c:
      if contract.issuer.name not in issuers.keys():
         issuers[contract.issuer.name] = 0
      if contract.reward / contract.volume > 200:
         issuers[contract.issuer.name] += contract.volume

   tickets = list()
   for k, v in issuers.items():
      if v:
         tickets += [k] * max(1, int(v / 10000))

   win = random.choice(tickets)

   u = Updates(datetime.now(), last.cachedUntil + timedelta(14), 'lottery', note=str(win))
   db.session.add(u)
   db.session.commit()

def pull_parties():
   """
   Updates any unnamed id found in the contracts table.
   Runs every two hours.

   """
   result = Parties.query.filter(Parties.name == None).all()
   if result:

      ids = [str(r.id) for r in result][:250]
      apir = api.eve.CharacterName(ids=','.join(ids)).characters
      for row in apir:
         r = Parties.query.get(row['characterID'])
         if not r:
            r = Parties(row['characterID'])

         r.name = row['name']
         db.session.add(r)
   
   new_update = Updates(datetime.now(), datetime.now()+timedelta(hours=2), 'parties')
   db.session.add(new_update)
   db.session.commit()

def update(force=False):
   """
   updates Contracts with a new pull if the cache timer is up or if forced

   if there's an api error, nothing is done, probably needs refined

   """
   last = Updates.query.filter(Updates.type=='contracts').order_by(db.desc(Updates.id)).first()
   if datetime.now() > last.cachedUntil or force:
      try:
         contracts = api.corp.Contracts()
         seconds = contracts._meta.cachedUntil - contracts._meta.currentTime
         new_update = Updates(datetime.now(), datetime.now()+timedelta(seconds=seconds), 'contracts')
         contracts = contracts.contractList

         for contract in contracts:
            c = Contracts.query.get(contract['contractID'])
            if c:
               c.update(contract)
            else:
               c = Contracts(contract)

            for id in c.ids():
               Parties(id)
            db.session.add(c)

         db.session.add(new_update)
         db.session.commit()
         #makeImage()
         return 'success'
      except Exception, e:
         return e.message
   else:
      return 'too early'

def resetdb():
   """
   Should only be used once, when the database is renewed
   Drops everything, creates everything, and then initializes
   the updates since my code will break without it.

   """
   db.drop_all()
   db.create_all()
   u = Updates(datetime.fromtimestamp(1), datetime.fromtimestamp(2), 'contracts')  
   bono = Updates(datetime.fromtimestamp(1), datetime.fromtimestamp(2), 'parties')
   db.session.add(u)
   db.session.add(bono)
   db.session.commit()
   worker()

def total_seconds(td):
   """
   returns total seconds from a timedelta since apparently i'm not using 2.7

   """
   return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6

def getTotalm3(length=None):
   """
   gets total volume moved over the last [length] days

   """
   total = 0
   contracts = Contracts.query.filter(Contracts.status=='Completed')
   if length:
      d = datetime.now() - timedelta(length)
      contracts = contracts.filter(Contracts.dateCompleted>=d)
   for contract in contracts:
      total += contract.volume
   return int(total)

def getNextUpdate():
   """
   gets next api update time

   """
   s = Updates.query.filter(Updates.type=='contracts').order_by(db.desc(Updates.id)).first().cachedUntil
   d = total_seconds(s - datetime.now())
   if d >= 0:
      next = strDuration(total_seconds(s - datetime.now()))
   else:
      next = None
   return next

def strTime(seconds):
   """
   prints the date in Month 00, 0000 format
   
   """
   months = [0,'January','Feburary','March',
             'April','May','June','July',
             'August','September','October',
             'November','December']
   t = time.gmtime(seconds)
   result = months[t[1]], str(t[2]), str(t[0])
   return ' '.join(result)

def strDuration(seconds):
   """
   prints the length of seconds in 0d 0h 0m 0s format

   """
   days = seconds / (3600*24)
   seconds -= days * 3600 * 24
   hours = seconds / 3600
   seconds -= hours * 3600
   minutes = seconds / 60
   seconds -= minutes * 60
   r = ''
   if days:
      r += str(days)+'d '
   if hours:
      r += str(hours)+'h '
   if minutes:
      r += str(minutes)+'m '
   r+= str(seconds)+'s'
   return r

@app.template_filter()
@evalcontextfilter
def humanize(eval_ctx, n):
   """
   gives numbers comma separators

   """
   if type(n) in [int, long, float]:
      s, ns = str(n).split('.')[0][::-1], ''
      for x in xrange(1,len(s)+1):
         ns = s[x-1]+ns
         if not x % 3 and len(s) > x: ns = ","+ns
   if eval_ctx:
      if eval_ctx.autoescape: ns = Markup(ns)
   return ns

@app.template_filter()
@evalcontextfilter
def pretty(eval_ctx, f):
   """
   cleans up percentages

   """
   if type(f) is float:
      r = str(int(f*100))+'%'

   if eval_ctx:
      if eval_ctx.autoescape: r = Markup(r)
   return r

@app.route('/')
def index():
   return 'Hi.'

@app.route('/dovi')
def jfcontracts():
   """
   First this attempts to update the contracts, then it'll grab 
   all Outstanding and InProgress contracts and all
   relevant information, then it'll sort through the contracts
   to grab the volume, reward, and station information to 
   store and display.

   """
   outstanding = list()
   stations = {60003760: {'name':"Jita",  'volume':0, 'reward':0, 'total':0},
               61000363: {'name':"6VDT-H",'volume':0, 'reward':0, 'total':0},
               61000101: {'name':"U-SOH2",'volume':0, 'reward':0, 'total':0},
               60014945: {'name':'1DH-SX','volume':0, 'reward':0, 'total':0},
               61000746: {'name':'K-6K16','volume':0, 'reward':0, 'total':0}}

   price, default = dict(), 275
   for id in stations.keys():
      price[id] = dict()
      for others in stations.keys():
         price[id][others] = default

   price[61000746][60014945] = 100
   price[60014945][61000746] = 100

   contracts = Contracts.query.filter(Contracts.status.in_(['Outstanding', 'InProgress']))
   contracts = contracts.filter(Contracts.type=="Courier")
   contracts = contracts.order_by(Contracts.contractID)

   for contract in contracts:
      start = contract.startStationID
      end = contract.endStationID
      if start in stations and end in stations.keys():
         stations[end]['volume'] += contract.volume
         stations[end]['reward'] += contract.reward
         stations[end]['total'] += 1
         if contract.status == 'InProgress':
            inp = True
         else:
            inp = False
         outstanding.append({'start': stations[start]['name'],
                             'startID': start,
                             'end': stations[end]['name'],
                             'endID': end,
                             'volume': contract.volume,
                             'reward': contract.reward,
                             'rate': math.ceil(contract.reward/contract.volume),
                             'age': strDuration(total_seconds(datetime.now() - contract.dateIssued)),
                             'price': price[start][end],
                             'inprogress': inp})

   vars = {'stations':    stations.items(),
           'outstanding': outstanding,
           'total':       getTotalm3(30),
           'next':        getNextUpdate() }

   return render_template('dovi.html', **vars)

@app.route('/dovi/stats')
def stats():
   """
   grabs all contracts within 30 days and shows all information on them

   """
   contracts = Contracts.query.filter(Contracts.status=='Completed')
   contracts = contracts.order_by(db.desc(Contracts.dateCompleted))
   contracts = contracts.filter(Contracts.type=="Courier")
   contracts = contracts.filter(Contracts.dateCompleted>=(datetime.now()-timedelta(30)))

   def subdate(days, query):
      cdate = datetime.now() - timedelta(days)
      contracts = query.filter(Contracts.dateCompleted>=cdate)
      isk = volume = duration = 0
      c24h = c48h = mt48h = 0
      for contract in contracts:
         isk += contract.reward
         volume += contract.volume
         duration += contract.duration()
         if contract.duration() < 3600 * 24:
            c24h += 1
         elif contract.duration() < 3600 * 48:
            c48h += 1
         else:
            mt48h += 1
      duration = duration / contracts.count()
      return {'isk':isk,
              'volume':volume,
              'duration':strDuration(duration),
              'lt24h':c24h,
              'lt48h':c48h,
              'gt48h':mt48h,
              'contracts':contracts.count()}

   results = list()
   for contract in contracts:
      r = contract.summary()
      r['durationStr'] = strDuration(r['duration'])
      r['dateCompletedStr'] = strTime(time.mktime(r['dateCompleted'].timetuple()))
      results.append(r)

   vars = {'stats':    results,
           'threeday': subdate(3, contracts),
           'sevenday': subdate(7, contracts),
           'everyday': subdate(30, contracts),
           'next':     getNextUpdate()}

   return render_template('stats.html', **vars)

@app.route('/dovi/lottery')
def lottery():
   """
   pulls and displays current valid tickets

   """
   last = Updates.query.filter(Updates.type=='lottery').order_by(db.desc(Updates.id)).first()
   r = Contracts.query.filter(Contracts.status=="Completed")
   r = r.filter(Contracts.dateCompleted>=last.requestTime)

   issuers = dict()
   for contract in r:
      if contract.issuer.name not in issuers.keys():
         issuers[contract.issuer.name] = 0
      if contract.reward / contract.volume > 200:
         issuers[contract.issuer.name] += contract.volume

   tickets = [[v, max(1, int(v / 10000)), i]  for i, v in issuers.items() if v]

   winners = Updates.query.filter(Updates.type=='lottery').order_by(db.desc(Updates.id))

   next_lottery = strDuration(total_seconds(winners[0].cachedUntil - datetime.now()))

   vars = dict(next = getNextUpdate(),
               next_lottery = next_lottery,
               contracts = r,
               winners = winners,
               tickets = sorted(tickets)[::-1] )

   return render_template('lottery.html', **vars)

@app.route('/dovi/update')
def updatepage():
   """
   force an update

   """
   update(force=True)
   return 'updated'

if __name__ == "__main__":
   if len(sys.argv) > 1:
      if sys.argv[1] == 'worker':
         while True:
            delay = worker()
            print 'waking in %ss' % delay
            if delay < 1:
               delay = 15
            time.sleep(delay)
      elif sys.argv[1] == 'db':
         pass
      elif sys.argv[1] == 'dbreset':
         resetdb()
   else:
      app.run(host="0.0.0.0",debug=True)
